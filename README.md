# README #

### What is this repository for? ###

This is a public repository for the **PAML lab** exercises for the **Workshop on Molecualr evolution** at the Marine Biological Laboratories (MBL) in Woods Hole, MA.

### Objectives ###

The objective of this activity is to help you understand how to use different codon models, and how to test for selection using PAML (and specifically the CODEML program). The activities are designed to build general analytical skills, and are just as relevant to analyses carried out using other software packages, such as HyPhy.

The tutorial is divided into 4 excercises.

1. Maximum likelihood estimation (by hand)

2. Sensitivity of ω

3. LRTs for alternative hypothises about temporal changes in selection pressure

4. Test for sites evolving by postive selection in the nef gene of HIV-2

### Contents ###
 
1. Book Chapter that decscribes each excercise (PDF)

2. 4 compressed archives (zip); 1 for each of the 4 excercises

3. PAML lab slides (PDF from 2016)

4. 2007 publciation by Ziheng Yang that descrivbes the PAML package


   
